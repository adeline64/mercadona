import React, {useEffect, useState} from "react";
import Cookies from 'js-cookie';
import JsonContext from "./JsonContext";
import jwtDecode from "jwt-decode";

export default function TokenContextProvider(props: { children: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; }) {

    const [token, setToken] = useState<string | undefined>(() => {
        const token = Cookies.get('TOKEN');
        if (token) {
            return token;
        }
        return undefined;
    });

    const handleSetToken = (token: string) => {
        setToken(token);
        Cookies.set('TOKEN', token);
    };

    return (
        // @ts-ignore
        // <JsonContext.Provider value={{ token, userId, roles, setToken: handleSetToken }}>
        <JsonContext.Provider value={{ token, setToken: handleSetToken }}>
            {props.children}
        </JsonContext.Provider>
    );
}