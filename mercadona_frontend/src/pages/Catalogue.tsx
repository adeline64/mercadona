import React, { useEffect, useContext, useState, ChangeEvent, FormEvent } from "react";
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import JsonContext from "../context/JsonContext";

interface Category {
    id: number;
    wording: string;
}

interface Product {
    id: number;
    wording: string;
    description: string;
    price: number;
    picture: string;
    category: Category;
}

export default function Catalogue() {

    const [products, setProducts] = useState<Product[]>([]);
    const [error, setError] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        // récupération des produits depuis l'API Django
        const { VITE_SERVER_ADDRESSES } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESSES}/api/products/`)
            .then((response) => setProducts(response.data))
            .catch((error) => setError(error.message));
    }, []);

    return (
        <Container>
            <Row>
                {products.map((product) => (
                    <Col key={product.id} sm={6} md={4} lg={3} className="mb-4">
                        <br/>
                        <Card>
                            <Card.Img variant="top" src={product.picture} />
                            <Card.Body>
                                <Card.Title>{product.wording}</Card.Title>
                                <br/>
                                <Card.Text>{product.description}</Card.Text>
                                <br/>
                                <Card.Text className="text-center">{product.price} €</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
        </Container>
    );
}