import React, { useEffect, useState, ChangeEvent, FormEvent } from "react";
import { Form, Button } from "react-bootstrap";
import axios from "axios";
import { useNavigate } from "react-router-dom";

interface FormControlElement extends HTMLInputElement {
    value: string;
}

export default function CategoryForm() {
    const [wording, setWording] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate();

    // @ts-ignore
    // const {token, setToken} = useContext(JsonContext);

    const handleSubmit = async (event: FormEvent) => {
        event.preventDefault();

        const productFormData = new FormData();
        productFormData.append("wording", wording);

        try {
            const { VITE_SERVER_ADDRESSES } = import.meta.env;

            await axios.post(`${VITE_SERVER_ADDRESSES}/api/categories/`, productFormData);
            navigate("/catalogue");
        } catch (error: unknown) {
            setError((error as Error).message);
        }
    };

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formProductLabel">
                <Form.Label>Libellé</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Entrer le libellé du produit"
                    value={wording}
                    onChange={(e: React.ChangeEvent<FormControlElement>) =>
                        setWording(e.target.value)
                    }
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                Ajouter
            </Button>
        </Form>
    );
}