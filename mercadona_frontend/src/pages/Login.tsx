import React, {useContext, useState} from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom"
import {Form, Button, Container, Row, Col} from "react-bootstrap";
import JsonContext from "../context/JsonContext";
import jwtDecode, {JwtPayload} from "jwt-decode";
import Cookies from 'js-cookie';

export default function LoginForm() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate()

    // @ts-ignore
    const {setToken} = useContext(JsonContext);

    function handleSubmit(event: { preventDefault: () => void; }) {

        event.preventDefault();

        const { VITE_SERVER_ADDRESSES} = import.meta.env;

        if (username && password) {
            axios
                .post(`${VITE_SERVER_ADDRESSES}/dj-rest-auth/login/`, {
                    username,
                    password,
                })
                .then((response) => {

                    const token = JSON.stringify(response.data.key);
                    Cookies.set('TOKEN', token);

                    setToken(token) // j'enregistre le token dans le context de l'application

                    navigate("/");

                })
                .catch((error) => {
                    if (error?.response?.status == 401) {
                        alert("Unauthorized access");
                    } else {
                        console.error(error);
                    }
                });
        } else {
            alert("Merci de vous connectez")
            navigate("/login")
        }
    }

    return (
        <Form className="app" id="stripe-login" method="POST" onSubmit={handleSubmit}>
            <Form.Group as={Col} md={6} controlId="formEmail">
                <Form.Label>Username:&nbsp;</Form.Label>
                <Form.Control
                    type="text"
                    name="username"
                    placeholder="username"
                    value={username}
                    onChange={(event) => setUsername(event.target.value)}
                />
            </Form.Group>

            <Form.Group as={Col} md={6} controlId="formPassword">
                <Form.Label>Password:&nbsp;</Form.Label>
                <Form.Control
                    type="password"
                    name="password"
                    placeholder="***********"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                Se connecter
            </Button>
        </Form>

    );
}