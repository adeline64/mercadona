from django.test import TestCase

from category.models import Category

# Create your tests here.
class CategoryTestCase(TestCase):

    def test_create_category(self):

        # Voir combien d'élément sont présent dans la db
        nbr_of_category_before_add = Category.objects.count()

        # Ajouter un objet dans la db
        new_category = Category()
        new_category.wording = "Mur"
        new_category.save()

        nbr_of_category_after_add = Category.objects.count()

        # valider que le nombre d'objet dans la db a été incrémenté de 1
        self.assertTrue(nbr_of_category_after_add == nbr_of_category_before_add + 1)
