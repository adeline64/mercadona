from django.db import models
from category.models import Category


# Create your models here.
class Product(models.Model):
    wording = models.CharField(max_length=50)
    description = models.TextField()
    price = models.FloatField()
    picture = models.FileField(upload_to='media/product_images/')
    category = models.ForeignKey(Category, null=False, on_delete=models.CASCADE, related_name="fk_id_category")

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'