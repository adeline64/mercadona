from django.contrib import admin

from product.models import Product


# Register your models here.
@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):

    list_display = ('wording', 'description', 'price', 'picture', 'category_name',)
    list_filter = ('wording', 'price', 'category__wording')

    def category_name(self, obj):
        return obj.category.wording

    category_name.short_description = 'Category'
