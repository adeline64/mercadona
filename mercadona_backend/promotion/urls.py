from rest_framework import routers

from promotion.views import PromotionViewSet

router = routers.DefaultRouter()
router.register('api/promotions', PromotionViewSet)