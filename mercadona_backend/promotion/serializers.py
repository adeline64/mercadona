from rest_framework import serializers

from promotion.models import Promotion


class PromotionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Promotion
        fields = ['date_of_begin', 'date_of_end', 'percentage', 'product', 'user']