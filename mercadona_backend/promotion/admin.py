from django.contrib import admin

from promotion.models import Promotion


# Register your models here.
@admin.register(Promotion)
class PromotionAdmin(admin.ModelAdmin):

    list_display = ('date_of_begin', 'date_of_end', 'percentage', 'product_name', 'user_name',)

    def product_name(self, obj):
        return obj.product.wording

    product_name.short_description = 'Product'
    #product_name.admin_order_field = 'product__wording'

    def user_name(self, obj):
        return obj.user.username

    user_name.short_description = 'User'
